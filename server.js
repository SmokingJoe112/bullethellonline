const express = require('express');
const socketIO = require('socket.io');
const http = require('http');
const fs = require('fs');

const app = express();

const port = 3000;

app.use(express.static(__dirname+'/Client'));

var server = http.createServer(app);

server.listen(port, () => {
    console.log(`Server listening on ${port}`);
});

const io = socketIO(server);

const defaultPlayerCd = 0.8;
const defaultPlayerSpd = 200;

const defaultMatchTime = 3;

var maxPlayers = 6;

var maps = ['X', 'RectPoint', 'HallSquared'];

var players = [];
var bullets = [];
var blocks = [];

var powerups = [];
var powerupSpawns = [];

var spawnPoints = [];
var scale = 10;

var selectMap = false;
var selectMaps = [];
var nextMap = 'X';

var timer = {
    minutes: 0,
    seconds: 0
};

const loadMap = function(m) {
    fs.readFile(`./Maps/${m}.json`, (err, jsonData) => {
	let parsedData = JSON.parse(jsonData);
	let map = parsedData.map;
	
	scale = parsedData.scale;

	spawnPoints = [];
	blocks = [];
	powerupSpawns = [];
	powerups = [];
	
	for (let y = 0; y < map.length; y ++) {
            for (let x = 0; x < map[y].length; x ++) {
		if (map[y][x] === 'S') {
                    spawnPoints.push({x: x*scale, y:y*scale});
		} else if (map[y][x] == 'DS'){
                    powerupSpawns.push({powerup: new Powerup(x*scale, y*scale, powerupSpawns.length, speedBoost, 'DS'), cooldown: 0, setCooldown: 20, spawned: false});
		} else if (map[y][x] == 'HC') {
                    powerupSpawns.push({powerup: new Powerup(x*scale, y*scale, powerupSpawns.length, fireRateBoost, 'HC'), cooldown: 0, setCooldown: 20, spawned: false});
		} else if (map[y][x] == 'TS') {
                    powerupSpawns.push({powerup: new Powerup(x*scale, y*scale, powerupSpawns.length, tripleShot, 'TS'), cooldown: 0, setCooldown: 20, spawned: false});
		} else if (map[y][x] == 'IV') {
                    powerupSpawns.push({powerup: new Powerup(x*scale, y*scale, powerupSpawns.length, invisibility, 'IV'), cooldown: 0, setCooldown: 20, spawned: false});
		} else if (map[y][x] == 'LZ') {
		    powerupSpawns.push({powerup: new Powerup(x*scale, y*scale, powerupSpawns.length, lazer, 'LZ'), cooldown: 0, setCooldown: 20, spawned: false});
		} else if (map[y][x] != 'X') {
                    blocks.push({x: x*scale, y: y*scale, w: scale, h: scale, key: map[y][x]});
		}
            }
	}
	io.emit('BlockUpdate', blocks);
	io.emit('PowerupUpdate', powerups);
    });
}
loadMap(maps[0]);

const rotateVec2 = function(angle, dir) {
    cs = Math.cos(angle);
    sn = Math.sin(angle);
    
    let nDir = {
	x: dir.x * cs - dir.y * sn,
	y: dir.x * sn + dir.y * cs
    };
    
    return nDir;
}

const snapAngle = function(angle, dir) {
    let tn = 0;

    if (dir.x && dir.y) {
	tn = Math.tan(dir.y/dir.x);

	tn = Math.round(tn/angle)*angle;
    } else if (dir.x === 0) {
	return {x: 0, y: dir.y};
    } else if (dir.y === 0) {
	return {x: dir.x, y: 0};
    }

    let nDir = rotateVec2(tn, {x: 1, y: 0});

    if (dir.x < 0) {
	nDir.x = Math.abs(nDir.x) * -1;
    } else if (dir.x > 0) {
	nDir.x = Math.abs(nDir.x);
    }

    if (dir.y < 0) {
	nDir.y = Math.abs(nDir.y) * -1;
    } else if (dir.y > 0) {
	nDir.y = Math.abs(nDir.y);
    }
    
    return nDir;
}

// Powerup functions
const speedBoost = function(p) {
    players[p].speed += 50;
    if (players[p].speed > 350) {
	players[p].speed = 350;
    }
}

const fireRateBoost = function(p) {
    players[p].setCooldown = 0.3;
}

const tripleShot = function(p) {
    players[p].tripleShot += 5;
}

const invisibility = function(p) {
    players[p].invisible += 10;
    racism = 2;
}

const lazer = function(p) {
    players[p].hasLazer += 1;
}

class Powerup {
    constructor (x, y, spawn, trigF, tpe) {
	this.triggerFunction = trigF;
	this.x = x;
	this.y = y;
	this.spawn = spawn;
	this.tpe = tpe;
    }

    trigger(p) {
	this.triggerFunction(p);
    }
}

io.on('connection', (sock) => {
    sock.emit('Welcome', blocks);
    sock.emit('PowerupUpdate', powerups);
    console.log("Connection made...");
    
    sock.on('JoinRequest', (name) => {
        if (players.length < maxPlayers) {
            let spawnPoint = Math.floor(Math.random()*spawnPoints.length);
            players.push({x: spawnPoints[spawnPoint].x, y: spawnPoints[spawnPoint].y, w: scale-1, h: scale-1, dir: {x: 0, y: 0}, speed: defaultPlayerSpd, tripleShot: 0, hasLazer: 0, invisible: 0, cooldown: 0, setCooldown: defaultPlayerCd, name: name, kills: 0, deaths: 0, id: sock.id});
	    if (selectMap == true) {
		sock.emit('JoinedGame');
	    } else {
		io.emit('GameEnd', {maps, selectMaps});
	    }
	}
    });
    
    sock.on('disconnect', () => {
        for (i = 0; i < players.length; i++) {
            if (players[i].id === sock.id) {
                players.splice(i, 1);
            }
        }
    });

    sock.on('Movement', (dir) => {
        for (i = 0; i < players.length; i++) {
            if (players[i].id === sock.id) {
                players[i].dir = dir;
		break;
            }
        }
    });

    sock.on('Shoot', (dir) => {
        let pos = {x: 0, y: 0};
        for (i = 0; i < players.length; i++) {
            if (players[i].id === sock.id) {
		if (players[i].cooldown <= 0) {
                    pos.x = players[i].x;
                    pos.y = players[i].y;
		    players[i].cooldown = players[i].setCooldown;
		    if (players[i].hasLazer > 0) {
			let d = snapAngle(1.5708, dir);
			bullets.push({x: pos.x-15, y: pos.y-15, w: scale*2.5, h: scale*2.5, dir: d, lifetime: 3, pierce: true, id: sock.id});
			players[i].hasLazer -= 1;
		    } else if (players[i].tripleShot > 0) {
			let dirOne = dir;
			let dirTwo = rotateVec2(0.25, dir);
			let dirThree = rotateVec2(-0.25, dir);
			
			bullets.push({x: pos.x + 5, y: pos.y + 5, w: scale/2, h: scale/2, dir: dirOne, lifetime: 3, id: sock.id});
			bullets.push({x: pos.x + 5, y: pos.y + 5, w: scale/2, h: scale/2, dir: dirTwo, lifetime: 3, id: sock.id});
			bullets.push({x: pos.x + 5, y: pos.y + 5, w: scale/2, h: scale/2, dir: dirThree, lifetime: 3, id: sock.id});
			players[i].tripleShot -= 1;
		    } else {
			bullets.push({x: pos.x + 5, y: pos.y + 5, w: scale/2, h: scale/2, dir: dir, lifetime: 3, id: sock.id});
		    }
		}
                break;
            }
        }
    });

    sock.on('ChatMsg', (msg) => {
	io.emit('BroadcastMsg', msg);
    });

    sock.on('VoteMap', (m) => {
	nextMap = m;
    });
});

const moveCollision = function (a, p, b) {
    let f = {
        x: a.x,
        y: a.y,
        w: a.w,
        h: a.h
    }

    if (a.y > b.y + b.h || a.x > b.x + b.w || a.y + a.h < b.y || a.x + a.w < b.x) {
        return a;
    } else {
        if (a.y + a.h > b.y && p.y + a.h <= b.y) {
            f.y = (b.y - a.h) - 0.3;
        }
        if (a.x + a.w > b.x && p.x + a.w <= b.x) {
            f.x = (b.x - a.w) - 0.3;
        }
        if (a.y < b.y + b.h && p.y >= b.y + b.h) {
            f.y = (b.y + b.h) + 0.3;
        }
        if (a.x < b.x + b.w && p.x >= b.x + b.w) {
            f.x = (b.x + b.w) + 0.3;
        }
        return f;
    }
}

const checkCollision = function (a, b) {
    aa = a;
    bb = b;
    aa.w = aa.w || scale;
    aa.h = aa.h || scale;
    bb.w = bb.w || scale;
    bb.h = bb.h || scale;
    if (aa.x < bb.x + bb.w &&
        aa.x + aa.w > bb.x &&
        aa.y < bb.y + bb.h &&
        aa.y + aa.h > bb.y) {
        return true;
    } else {
        return false;
    }
}

let lastTime = Date.now();
const update = function() {
    let ts = Date.now();
    let dt = (ts - lastTime)/1000;
    lastTime = ts;
    
    timer.seconds -= dt;
    if (timer.seconds <= 0) {
        timer.minutes -= 1;
        timer.seconds += 60;
    }
    
    if (timer.minutes < 0) {
	players = [];
	
	if (!selectMap) {
	    timer.minutes = defaultMatchTime;
	    timer.seconds = 0;
	    loadMap(nextMap);
	    io.emit('EndMapSelect');
	} else {
	    let rIndOne = Math.floor((Math.random()*maps.length));
	    let rIndTwo = rIndOne;
	    let rIndThree = rIndTwo;
	    while (rIndOne === rIndTwo) {
		rIndTwo = Math.floor(Math.random()*maps.length);
	    }
	    
	    while (rIndTwo === rIndThree || rIndOne === rIndThree) {
		rIndThree = Math.floor(Math.random()*maps.length);
	    }
	    selectMaps = [rIndOne, rIndTwo, rIndThree];
	    io.emit('GameEnd', {maps, selectMaps});
	    nextMap = maps[rIndOne];
	    
	    timer.minutes = 0;
	    timer.seconds = 10;
	}
	
	selectMap = !selectMap;
    }
    
    io.emit('TimerUpdate', timer);
    
    for (i = 0; i < powerupSpawns.length; i ++) {
	if (powerupSpawns[i].spawned === false) {
	    if (powerupSpawns[i].cooldown > 0) {
		powerupSpawns[i].cooldown -= dt;
	    } else {
		powerups.push(powerupSpawns[i].powerup);
		powerupSpawns[i].spawned = true;
		io.emit('PowerupUpdate', powerups);
	    }
	}
    }
    
    for (i = 0; i < players.length; i ++) {
	if (players[i].cooldown >= 0) {
	    players[i].cooldown -= dt;
	}

	if (players[i].invisible >= 0) {
	    players[i].invisible -= dt;
	}
	
        let nextPos = {
            x: players[i].x + players[i].dir.x * dt * players[i].speed,
            y: players[i].y + players[i].dir.y * dt * players[i].speed,
            w: players[i].w,
            h: players[i].h
        }

        for (o = 0; o < blocks.length; o++) {
            mvPos = moveCollision(nextPos, players[i], blocks[o]);
            if (!checkCollision(mvPos, blocks[o])) {
                nextPos = mvPos;
            } else {
                console.log('collision');
            }
        }

        players[i].x = nextPos.x;
        players[i].y = nextPos.y;

	for (p = 0; p < powerups.length; p++) {
	    if (checkCollision(powerups[p], players[i])) {
		powerups[p].trigger(i);
		powerupSpawns[powerups[p].spawn].cooldown = powerupSpawns[powerups[p].spawn].setCooldown; 
		powerupSpawns[powerups[p].spawn].spawned = false;
		powerups.splice(p, 1);
		io.emit('PowerupUpdate', powerups);
		break;
	    }
	}
	
        for (b = 0; b < bullets.length; b ++) {
            if (bullets[b].id != players[i].id) {
                if (checkCollision(bullets[b], players[i])) {
                    let killer = 'Ghost';
		    players[i].deaths+=1;
                    for (n = 0; n < players.length; n++) {
                        if (players[n].id === bullets[b].id) {
                            killer = players[n].name;
			    players[n].kills += 1;
                            break;
                        }
                    }
                    io.emit('Kill', `${killer} X ${players[i].name}`);

                    let spawnPoint = Math.floor(Math.random()*spawnPoints.length);
                    players[i].x = spawnPoints[spawnPoint].x;
                    players[i].y = spawnPoints[spawnPoint].y;
		    players[i].speed = defaultPlayerSpd;
		    players[i].setCooldown = defaultPlayerCd;
                }
            }
        }
    }

    let deadBullets = [];
    for (i = 0; i < bullets.length; i ++) {
        bullets[i].x += bullets[i].dir.x * dt * 300;
        bullets[i].y += bullets[i].dir.y * dt * 300;
        bullets[i].lifetime -= dt;
        if (bullets[i].lifetime <= 0) {
            deadBullets.push(i);
        }

	if (!bullets[i].pierce) {
            for (b = 0; b < blocks.length; b ++) {
		if (checkCollision(bullets[i], blocks[b])) {
                    deadBullets.push(i);
                break;
		}
            }
	}
    }

    for (i = 0; i < deadBullets.length; i ++) {
        bullets.splice(deadBullets[i], 1);
    }

    io.emit('PlayerUpdate', players);
    io.emit('BulletUpdate', bullets);
    setTimeout(update, 0);
}
update(lastTime);
