### Bullet Hell Online
Bullet Hell Online is a fun arena style top-down shooter, where the aim of the game is to blast your friends and collect cool powerups!
This is the first fun multiplayer game I made. Currently BHO is buggy and the UI is terrible, and development has stopped because I am focussing on BHO's successor Squib-Online.

### Setup
*Nodejs is required to run BHO.*  
Clone the project into your desired directory  
Next install required packages. BHO is powered by socket.io for networking and uses express to serve files.  
    `$cd bullethellonline`  
    `$npm install`  

The server can then be started with  
    `npm run dev` - Runs a nodemon process  
    `npm run start` - Runs a standard Nodejs process  

### Playing the game
*BHO was tested for Firefox and Chrome, using other browsers may cause errors.*  
Once the server is running navigate to `<host ip>:3000` in your web-browser.  
Enter your username and shoot your friends!

***Rounds***  
The timer at the top of the screen displays how much time is left in a round.  
When a round ends a screen will be displayed where you can vote on a new map.  
After a few seconds a new match will start with the new map loaded.  
*This screen needs a UI overhaul*  




