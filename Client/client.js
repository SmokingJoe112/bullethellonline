const ctx = document.querySelector('#gameCanvas').getContext('2d');
const chatArea = document.querySelector('#chatArea');
chatArea.scrollTop = chatArea.scrollHeight;
const chatInput = document.querySelector('#chatInput');
const chatForm = document.querySelector('#chatForm');
const scoreboard = document.querySelector('#scoreboard');

var images = new Map();

const blockIMG = document.createElement('img');
blockIMG.src = '/res/block.png';
images.set('0', blockIMG);

const speedBoostIMG = document.createElement('img');
speedBoostIMG.src = '/res/SpeedBoost.png';
images.set('DS', speedBoostIMG);

const fireRateBoostIMG = document.createElement('img');
fireRateBoostIMG.src = '/res/FireRateBoost.png';
images.set('HC', fireRateBoostIMG);

const tripleShotIMG = document.createElement('img');
tripleShotIMG.src = '/res/TripleShot.png';
images.set('TS', tripleShotIMG);

const invisibilityIMG = document.createElement('img');
invisibilityIMG.src = '/res/Invisibility.png';
images.set('IV', invisibilityIMG);

const laserIMG = document.createElement('img');
laserIMG.src = '/res/Laser.png';
images.set('LZ', laserIMG);

var sock = io();

var gameState = 'menu';

var scoreEntries = [];

var players = [];
var bullets = [];
var powerups = [];

var map = [];
var selectMaps = [];

var username = '';

var cooldown = 0;

var timer;

var menuButtons = [];

var voteMapButtons = [];

class Button {
    constructor(x, y, w, h, text, callback) {
	this.x = x;
	this.y = y;
	this.w = w;
	this.h = h;
	this.text = text;
	this.callback = callback;
	this.flash = 0;
    }

    trigger() {
	     this.callback();
    }
}

menuButtons.push(new Button(ctx.canvas.width/2-75, ctx.canvas.height/2, 150, 60, 'Play', () => {
    if (username.length>0) {
        sock.emit('JoinRequest', username);
    } else {
        addChatText('Error: Please enter your username');
    }
}));

const sendVoteMap = function(i) {
    sock.emit('VoteMap', selectMaps[i]);
}

for (i = 0; i < 3; i++) {
    voteMapButtons.push(new Button(0, 0, 150, 60, 'VoteMap', () => {
	sendVoteMap(i);
    }));
}

chatForm.addEventListener('submit', (e) => {
    e.preventDefault();
    if (gameState == 'play') {
	sock.emit('ChatMsg', `${username}: ${chatInput.value}`);
	chatInput.value = '';
    }
});

sock.on('Welcome', (blocks) => {
    console.log('Connected to server..');
    map = blocks;
});

sock.on('BlockUpdate', (b) => {
    console.log('MapUpdate');
    map = [];
    map = b;
});

sock.on('JoinedGame', () => {
    gameState = "play";
    chatArea.value = '';
});

sock.on('BroadcastMsg', (msg) => {
    addChatText(msg);
});

sock.on('PowerupUpdate', (pups) => {
    powerups = pups;
});

sock.on('TimerUpdate', (t) => {
    timer = t;
});

sock.on('PlayerUpdate', (plist) => {
    players = plist;

    for (i = 0; i < players.length; i++) {
	let exists = false;
	let index = 0;
	if (scoreEntries.length >= 1) {
	    for (s = 0; s < scoreEntries.length; s ++) {	
		if (scoreEntries[s][0] === players[i].id) {
               	    exists = true;
		    index = s;
		    break;
	        }
	    } 
        }
    
        if (!exists) {
	    addScoreEntry(players[i]);
	} else {
	    // Change Entry
	    scoreEntries[index][1].innerHTML = players[i].name;
	    scoreEntries[index][2].innerHTML = players[i].kills;
	    scoreEntries[index][3].innerHTML = players[i].deaths;
	}
    }

    if (gameState == 'play') {
	let e = 0;
	while (e < scoreEntries.length) {
	    hasPlayer = false;
	    
	    for (p = 0; p < players.length; p ++) {
		if (scoreEntries[e][0] == players[p].id) {
		    hasPlayer = true;
		}
	    }
	    
	    if(!hasPlayer) {
		scoreboard.deleteRow(e+1);
		scoreEntries.splice(e,1);
	    } else {
		e++;
	    }
	}
    }
});

sock.on('BulletUpdate', (blist) => {
    bullets = blist;
});

sock.on('Kill', (text) => {
    addChatText(text);
});

sock.on('GameEnd', (end) => {
    gameState = 'endscreen';
    selectMaps = [];
    console.log(end.maps, end.selectMaps);
    for (i = 0; i < end.selectMaps.length; i++) {
	selectMaps.push(end.maps[end.selectMaps[i]]);
    }
});

sock.on('EndMapSelect', () => {
    gameState = 'menu';
});

const addChatText = function(text) {
    chatArea.value = chatArea.value + `\n${text}`;
    chatArea.scrollTop = chatArea.scrollHeight;
}

const addScoreEntry = function(p) {
    let row = scoreboard.insertRow(-1);
    let uname = row.insertCell(0);
    let kills = row.insertCell(1);
    let deaths = row.insertCell(2);

    uname.innerHTML = 'N';
    
    scoreEntries.push([p.id, uname, kills, deaths, row]);
}

const checkButton = function(b, p) {
    if (p.x < b.x + b.w && p.x > b.x && p.y > b.y && p.y < b.y + b.h) {
	return true;
    } else {
	return false;
    }
}

var inputs = [];
document.addEventListener('keydown', (kd) => {
    if (!inputs.includes(kd.code)) {
	inputs.push(kd.code);
    }
    
    if (gameState === 'menu') {
	if (kd.key != 'F5') {
	    kd.preventDefault();
	}
	
	if (kd.key === 'Backspace') {
            username = username.slice(0, -1);
        } else if(kd.key.length <= 1 && username.length<7) {
            username = username + kd.key;
        }
    }
});

document.addEventListener('keyup', (ku) => {
    for (i = 0; i < inputs.length; i++) {
        if (inputs[i] === ku.code) {
            inputs.splice(i, 1);
        }
    }
});

document.addEventListener('click', (c) => {
    if (gameState === 'menu') {
	for (i = 0; i < menuButtons.length; i++) {
	    let rect = ctx.canvas.getBoundingClientRect();
	    let p = {
		x: c.clientX-rect.left,
		y: c.clientY-rect.top
	    };
	    if (checkButton(menuButtons[i], p)) {
		menuButtons[i].trigger();
		menuButtons[i].flash += 5;
	    }
	}
    } else if (gameState === 'endscreen') {
	for (i = 0; i < voteMapButtons.length; i++) {
	    let rect = ctx.canvas.getBoundingClientRect();
	    let p = {
		x: c.clientX-rect.left,
		y: c.clientY-rect.top
	    };

	    if (checkButton(voteMapButtons[i], p)) {
		voteMapButtons[i].trigger();
		voteMapButtons[i].flash += 5;
	    }
	}
    }
});

const checkInput = function() {
    let dir = {x: 0, y: 0};
    // Up
    if (inputs.includes('KeyW')) {
	dir.y -= 1;
    }
    
    // Down
    if (inputs.includes('KeyS')) {
	dir.y += 1;
    }
    
    // Left
    if (inputs.includes('KeyA')) {
	dir.x -= 1;
    }
    
    // Right
    if (inputs.includes('KeyD')) {
	dir.x += 1;
    }
    
    let dirMagnitude = (dir.x * dir.x) + (dir.y * dir.y);
    if (dirMagnitude != 0) {
        dir.x *= 1/dirMagnitude;
        dir.y *= 1/dirMagnitude;
    }
    
    sock.emit('Movement', dir);
    
    let shootDir = {x: 0, y: 0};
    
    // Up
    if (inputs.includes('ArrowUp')) {
	shootDir.y -= 1;
    }
    
    // Down
    if (inputs.includes('ArrowDown')) {
	shootDir.y += 1;
    }
    
    // Left
    if (inputs.includes('ArrowLeft')) {
	shootDir.x -= 1;
    }
    
    // Right
    if (inputs.includes('ArrowRight')) {
	shootDir.x += 1;
    }
    
    if (!(shootDir.x == 0 && shootDir.y == 0)) {
        if (cooldown <= 0) {
            let shootDirMag = (shootDir.x * shootDir.x) + (shootDir.y * shootDir.y);
            if (shootDirMag != 1) {
                shootDir.x *= 1/shootDirMag;
                shootDir.y *= 1/shootDirMag;
            }
            shootDir.x += dir.x * 0.5;
            shootDir.y += dir.y * 0.5;
	    
            sock.emit('Shoot', shootDir);
        }
    }
}

let lastTime = 0;
const draw = function(ts) {
    dt = (ts-lastTime)/1000;
    lastTime = ts;
    
    ctx.clearRect(0,0,ctx.canvas.width, ctx.canvas.height);
    if (gameState == 'play') {
	ctx.font = '15px Arial';
	ctx.textAlign = 'center';
	
	for (p = 0; p < powerups.length; p ++) {
            ctx.drawImage(images.get(powerups[p].tpe), powerups[p].x, powerups[p].y);
	}
	
	for (i = 0; i < players.length; i++) {
            if (players[i].id === sock.id) {
		ctx.fillStyle = "blue";
		cooldown = players[i].cooldown;
            } else {
		ctx.fillStyle = "black";
            }
	    if (players[i].invisible > 0) {
		ctx.fillStyle = "#ebf5fb";
		ctx.fillRect(players[i].x, players[i].y, players[i].w, players[i].h);
	    } else {
		ctx.fillRect(players[i].x, players[i].y, players[i].w, players[i].h);
		
		ctx.fillStyle = '#000';
		ctx.fillText(players[i].name, players[i].x+players[i].w/2, players[i].y-1);
	    }
	}
	
	ctx.fillStyle = "red";
	for (i = 0; i < bullets.length; i++) {
            ctx.fillRect(bullets[i].x, bullets[i].y, bullets[i].w, bullets[i].h);
	}
	
	for (i = 0; i < map.length; i++) {
            ctx.fillStyle = '#006699';
            ctx.fillRect(map[i].x, map[i].y, map[i].w, map[i].h);
	}
	
	cooldown -= dt;
	if (cooldown < 0) {
            cooldown = 0;
	}
	checkInput();
	
	if (timer) {
            ctx.font = '25px Arial';
            ctx.fillStyle = '#000';
	    
	    let sec = `${Math.floor(timer.seconds)}`;
	    if (sec <= 9) {
		sec = '0' + sec;
	    }
            ctx.fillText(`${timer.minutes}:${sec}`, ctx.canvas.width/2, 30);
	}
	
    } else if (gameState === 'menu') {
	ctx.fillStyle = '#AED6F1';
	ctx.fillRect(0, 0, ctx.canvas.width, ctx.canvas.height);

	ctx.textAlign='center';
        ctx.font = '40px Arial';
        for (i = 0; i < menuButtons.length; i++) {
	    if (menuButtons[i].flash > 0) {
		ctx.fillStyle = '#99FFFF';
		menuButtons[i].flash -= 1;
	    } else {
		ctx.fillStyle = '#007799';
	    }
            ctx.fillRect(menuButtons[i].x, menuButtons[i].y, menuButtons[i].w, menuButtons[i].h);
            ctx.fillStyle = '#000';
            ctx.fillText(menuButtons[i].text, menuButtons[i].x + menuButtons[i].w/2, menuButtons[i].y + menuButtons[i].h-(menuButtons[i].h/4));
        }
	
	ctx.fillStyle = '#007799';
	
        ctx.fillRect(0, ctx.canvas.height-70, ctx.canvas.width, 70);
        ctx.fillStyle = '#000';
        if (username.length > 0) {
            ctx.fillText(username, ctx.canvas.width/2, ctx.canvas.height-20);
        } else {
            ctx.fillText('Type to enter "Username"', ctx.canvas.width/2, ctx.canvas.height-20);
        }
    } else if (gameState === 'endscreen') {
	ctx.font = '15px Arial';
	ctx.fillStyle = '#006699';
	ctx.fillRect(30, 30, ctx.canvas.width-60, ctx.canvas.height-60);
	ctx.fillStyle = '#000';
	ctx.fillText('"Click to continue..."', ctx.canvas.width/2, ctx.canvas.height-65);

	ctx.font = '30px Arial';
	for (i - 0; i < selectMaps.length; i++) {
	    voteMapButtons[i].x = ctx.canvas.width/2 - 90;
	    voteMapButtons[i].y = 50+(i*55);
	    voteMapButtons[i].w = 180;
	    voteMapButtons[i].h = 45;
	    if (voteMapButtons[i].flash < 0) {
		ctx.fillStyle = '#007799';
	    } else {
		ctx.fillStyle = '#99FFFF';
		voteMapButtons[i].flash -= 1;
	    }
	    ctx.fillRect(voteMapButtons[i].x, voteMapButtons[i].y, voteMapButtons[i].w, voteMapButtons[i].h);
	    ctx.fillStyle = '#000'
	    ctx.fillText(selectMaps[i], ctx.canvas.width/2, voteMapButtons[i].y+30);
	}
    }
    requestAnimationFrame(draw);
}
requestAnimationFrame(draw);
