const spawnIMG = document.createElement('img');
spawnIMG.src = '/res/spawn.png';

const blockIMG = document.createElement('img');
blockIMG.src = '/res/block.png';

const speedBoostIMG = document.createElement('img');
speedBoostIMG.src = '/res/SpeedBoost.png';

const fireRateBoostIMG = document.createElement('img');
fireRateBoostIMG.src = '/res/FireRateBoost.png';

const tripleShotIMG = document.createElement('img');
tripleShotIMG.src = '/res/TripleShot.png';

const invisibilityIMG = document.createElement('img');
invisibilityIMG.src = '/res/Invisibility.png';

const laserIMG = document.createElement('img');
laserIMG.src = '/res/Laser.png';

const blocks = [{key: 'S', img: spawnIMG}, {key: '0', img: blockIMG}, {key: 'HC', img: fireRateBoostIMG}, {key: 'DS', img: speedBoostIMG}, 
                {key: 'TS', img: tripleShotIMG}, {key: 'IV', img: invisibilityIMG}, {key: 'LZ', img: laserIMG}];

const eCnv = document.querySelector('#editorCanvas');
const pCnv = document.querySelector('#pickerCanvas');

const eCtx = eCnv.getContext('2d');
const pCtx = pCnv.getContext('2d');

const scaleInp = document.querySelector('#scaleInput');

var scale = scaleInp.value;

var selectedBlock = 0;

var board = [];

const saveMap = function () {
    let parsedMap = [];
    let min = {
        x: Number.MAX_VALUE,
        y: Number.MAX_VALUE
    }
    let max = {
        x: Number.MIN_VALUE,
        y: Number.MIN_VALUE
    }

    for (let i = 0; i < board.length; i ++) {
        if (board[i].x < min.x) {
            min.x = board[i].x;
        }
        if (board[i].y < min.y) {
            min.y = board[i].y;
        }
        if (board[i].x > max.x) {
            max.x = board[i].x;
        }
        if (board[i].y > max.y) {
            max.y = board[i].y;
        }
    }

    let offset = {
        x: 0 - min.x,
        y: 0 - min.y
    }
    

    for (let y = 0; y <= max.y + offset.y; y++) {
        let row = []
        for (let x = 0; x <= max.x + offset.x; x++) {
            row.push('X');
        }
        parsedMap.push(row);
    }

    for (let i = 0; i < board.length; i ++) {
        parsedMap[board[i].y + offset.y][board[i].x + offset.x] = board[i].block.key;
    }

    let jsonObj = {
        map: parsedMap,
        scale: scale
    }
    let data = 'data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(jsonObj));
    let e = document.createElement('a');
    e.setAttribute('href', data);
    e.setAttribute('download', 'Map.json');
    document.body.appendChild(e);
    e.click();
    e.remove();
}

document.addEventListener('keydown', (k) => {
    if (k.key === 'J') {
        saveMap();
    }
});

eCnv.addEventListener('click', (e) => {
    let rect = eCnv.getBoundingClientRect();
    let x = Math.floor((e.clientX-rect.left)/scale);
    let y = Math.floor((e.clientY-rect.top)/scale);

    if (selectedBlock === 'delete') {
        for (i = 0; i < board.length; i++) {
            if (board[i].x === x && board[i].y === y) {
                board.splice(i,1);
                break;
            }
            console.log('Delete');
        }
    } else {
        for (i = 0; i < board.length; i++) {
            if (board[i].x === x && board[i].y === y) {
                board.splice(i,1);
                break;
            }
        }
        board.push({x: x, y: y, block: blocks[selectedBlock]});
    }
}); 

pCnv.addEventListener('click', (e) => {
    let rect = pCnv.getBoundingClientRect();
    let x = Math.floor((e.clientX-rect.left)/scale);
    let y = Math.floor((e.clientY-rect.top)/scale);
    let indx = x+(y*pCnv.width/scale);
    if (indx >= blocks.length) {
        selectedBlock = 'delete';
    } else {
        selectedBlock = indx;
    }
});

const draw = function() {
    
    eCtx.clearRect(0, 0, eCnv.width, eCnv.height);
    pCtx.clearRect(0, 0, pCnv.width, pCnv.height);

    eCtx.beginPath();
    for (x = 0; x < eCnv.width/scale; x++) {
        for (y = 0; y < eCnv.height/scale; y++) {
            eCtx.fillStyle = 'black';
            eCtx.rect(x*scale, y*scale, scale, scale);
        }
    }
    eCtx.stroke();

    for (b = 0; b < blocks.length; b++) {
        let x = Math.floor(b%(pCnv.width/scale));
        let y = Math.floor(b/(pCnv.width/scale));
        pCtx.drawImage(blocks[b].img,x*scale, y*scale, scale, scale);
    }

    for (i = 0; i < board.length; i++) {
        eCtx.drawImage(board[i].block.img, board[i].x*scale, board[i].y*scale, scale, scale);
    }

    requestAnimationFrame(draw);
}
requestAnimationFrame(draw);
